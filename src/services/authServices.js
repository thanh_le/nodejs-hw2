const bcrypt = require('bcrypt');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const ROUND = '$2b$10$IfCuNZixdJa6EsW2MaVM0u';
const TOKEN_KEY = 'RESTFULAPIs';

const loginService = async (username, password) => {
  if (!username || !password) throw Error('Data is invalid!');

  const user = await User.findOne({
    $and: [
      {username: username},
      {hash_password: bcrypt.hashSync(password, ROUND)},
    ],
  });

  if (!user) throw Error('User not found!');

  const token = jwt.sign(
      {
        _id: user._id,
        username: user.username,
      },
      TOKEN_KEY,
      {
        expiresIn: '2h',
      },
  );
  return token;
};

const registerService = async (username, password) => {
  try {
    if (!username || !password) throw Error('Data is invalid!');

    const user = await User.findOne({username: username});
    if (user) throw Error('User already exsit!');

    const newUser = new User({
      username,
      hash_password: bcrypt.hashSync(password, ROUND),
      createdDate: Date().now,
    });

    await newUser.save();
  } catch (err) {
    throw err;
  }
};

module.exports = {loginService, registerService};
