const User = require('../models/user');
const Note = require('../models/note');

const getNoteService = async (noteId) => {
  if (!noteId) throw Error('Data is invalid!');

  const note = await Note.findById(noteId);
  if (!note) throw Error('Note not found!');
  return note;
};

const postUpdateNoteService = async (noteId, text) => {
  if (!noteId || !text) throw Error('Data is invalid!');
  Note.findByIdAndUpdate(
      {_id: noteId},
      {text: text, updatedDate: Date.now().toString()},
      (err, note) => {
        if (err) throw err;
      },
  );
};

const patchCheckNoteService = (noteId) => {};

const deleteNoteService = async (noteId) => {
  if (!noteId) throw Error('Data is invalid!');
  Note.deleteOne({_id: noteId}, (err, note) => {
    if (err) throw err;
  });
};

const getNoteListService = async (userId, offset, limit) => {
  if ((!userId, !offset, !limit)) throw Error('Data is invalid!');
  const notes = await Note.find({userId: userId});
  if (!notes) {
    throw Error('Notes not found!');
  }

  return {
    offset,
    limit,
    count: count,
    notes: notes,
  };
};

const postCreateNoteService = async (userId, text) => {
  if (!userId || !text) throw Error('Data is invalid!');

  const user = await User.findById(userId);
  if (!user) throw Error('User not found!');

  const note = new Note({
    text,
    createdDate: Date.now(),
    updatedDate: '',
    userId: user._id,
  });
  note.save((err, result) => {
    if (err) throw err;
  });
};

module.exports = {
  getNoteService,
  postUpdateNoteService,
  patchCheckNoteService,
  deleteNoteService,
  getNoteListService,
  postCreateNoteService,
};
