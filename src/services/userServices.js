const bcrypt = require('bcrypt');
const User = require('../models/user');
const ROUND = '$2b$10$IfCuNZixdJa6EsW2MaVM0u';

const getUserService = async (userId) => {
  if (!userId) throw Error('Token is invalid!');
  const user = await User.findById(userId);
  if (!user) throw Error('User not found!');
  user.hash_password = '';
  return user;
};

const updateUserService = async (userId, oldPassword, newPassword) => {
  if (!userId || !oldPassword || !newPassword) throw Error('Data is invalid!');

  const user = await User.findOne({
    $and: [
      {_id: userId},
      {hash_password: bcrypt.hashSync(oldPassword, ROUND)},
    ],
  });

  if (!user) throw Error('User not found!');

  await User.findOneAndUpdate(user, {
    hash_password: bcrypt.hashSync(newPassword, ROUND),
  });
};

const deleteUserService = async (userId) => {
  if (!userId) throw Error('Data is invalid!');
  const user = await User.findById(userId);
  if (!user) throw Error('User not found!');

  await User.findOneAndDelete(user);
};

module.exports = {getUserService, updateUserService, deleteUserService};
