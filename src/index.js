const express = require('express');
const router = require('./routes/app');
const cors = require('cors');
const bodyParser = require('body-parser');
const dbConnection = require('./db/connection');
const PORT = process.env.PORT || 8080;
const app = express();

app.use(express.json());
app.use(cors());
app.use(bodyParser.json());
app.use((req, res, next) => {
  next();
});

router(app);

app.listen(process.env.PORT, () => {
  console.log(`App is listening at port ${PORT}`);
});
dbConnection();
