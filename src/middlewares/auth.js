const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {
  const token =
    req.body.token || req.headers.jwt_token || req.headers['x-access-token'];

  if (!token) {
    return res.status(401).send('A token is required for authentication');
  }
  jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
    if (err) return res.status(401).send('Invalid Token');
    req.user = user;
    next();
  });
};

module.exports = verifyToken;
