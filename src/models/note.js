const mongoose = require('mongoose');

const schema = new mongoose.Schema(
    {
      _id: {
        type: mongoose.Schema.Types.ObjectId,
        index: true,
        required: true,
        auto: true,
      },
      text: String,
      createdDate: String,
      updatedDate: String,
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        require: true,
      },
      completed: Boolean,
    },
    {collection: 'note'},
);

const Note = mongoose.model('Note', schema);
module.exports = Note;
