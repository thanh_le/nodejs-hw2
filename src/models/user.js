const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const schema = new mongoose.Schema(
    {
      _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        auto: true,
      },
      username: {type: String, trim: true},
      hash_password: {type: String, required: true},
      createdDate: {type: String, default: Date.now().toString()},
      token: {type: String, trim: true, default: ''},
    },
    {collection: 'user'},
);

schema.methods.comparePassword = function(password) {
  return bcrypt.compareSync(password, this.hash_password);
};

const User = mongoose.model('User', schema);
module.exports = User;
