const {loginService, registerService} = require('../services/authServices');

const postLogin = async (req, res) => {
  try {
    const {username, password} = req.body;
    const token = await loginService(username, password);
    return res.status(200).json({message: 'Success', jwt_token: token});
  } catch (err) {
    return res.status(400).json({message: err});
  }
};

const postRegister = async (req, res) => {
  try {
    const {username, password} = req.body;
    await registerService(username, password);

    return res.status(200).json({message: 'Success'});
  } catch (err) {
    return res.status(400).json({message: err});
  }
};

module.exports = {postLogin, postRegister};
