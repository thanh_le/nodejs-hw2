const jwt = require('jsonwebtoken');
const TOKEN_KEY = 'RESTFULAPIs';
const {
  getUserService,
  updateUserService,
  deleteUserService,
} = require('../services/userServices');

const getUser = async (req, res) => {
  try {
    console.log(req.user);
    const {_id} = req.user;
    const user = await getUserService(_id);

    return res.status(200).json({
      user: {
        _id: user._id,
        username: user.username,
        createdDate: user.createdDate,
      },
    });
  } catch (err) {
    return res.status(400).json({message: err});
  }
};

const updateUser = async (req, res) => {
  try {
    const {_id} = jwt.verify(req.headers.jwt_token, TOKEN_KEY);
    const {oldPassword, newPassword} = req.body;

    await updateUserService(_id, oldPassword, newPassword);
    return res.status(200).json({message: 'Success'});
  } catch (err) {
    return res.status(400).json({message: err});
  }
};

const deleteUser = async (req, res) => {
  try {
    const {_id} = jwt.verify(req.headers.jwt_token, TOKEN_KEY);
    await deleteUserService(_id);
    return res.status(200).json({message: 'Success'});
  } catch (err) {
    return res.status(400).json({message: err});
  }
};

module.exports = {getUser, updateUser, deleteUser};
