const {
  getNoteService,
  postUpdateNoteService,
  patchCheckNoteService,
  deleteNoteService,
  getNoteListService,
  postCreateNoteService,
} = require('../services/noteServices');

const getNote = async (req, res) => {
  const id = req.body.id;
  const note = await getNoteService(id);
  if (!note) return res.status(200).json({message: 'Note not found!'});
  res.status(200).json(note);
};

const postUpdateNote = async (req, res) => {
  const id = req.params.id;
  const text = req.body.text;
  await postUpdateNoteService(id, text);
};

const patchCheckNote = async () => {
  const id = req.params.id;

  await patchCheckNoteService(id);
};

const deleteNote = async (req, res) => {
  try {
    const id = req.params.id;
    await deleteNoteService(id);
  } catch (err) {
    res.status(400).json({message: err});
  }
};

const getNoteList = async (req, res) => {
  try {
    const {_id} = req.user;
    const result = await getNoteListService(_id);
    res.status(200).send(result);
  } catch (err) {
    return res.status(400).json({message: err});
  }
};

const postCreateNote = async (req, res) => {
  try {
    const {_id} = req.user;
    const text = req.body.text;
    await postCreateNoteService(_id, text);
  } catch (err) {
    return res.status(400).json({message: err});
  }
};

module.exports = {
  getNote,
  postUpdateNote,
  patchCheckNote,
  deleteNote,
  getNoteList,
  postCreateNote,
};
