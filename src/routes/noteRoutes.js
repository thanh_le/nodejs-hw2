const express = require('express');
const router = new express.Router();
const NoteController = require('../controllers/NoteControllers');

router.get('/:id', NoteController.getNote);
router.put('/:id', NoteController.postUpdateNote);
router.patch('/:id', NoteController.patchCheckNote);
router.delete('/:id', NoteController.deleteNote);
router.get('/', NoteController.getNoteList);
router.post('/', NoteController.postCreateNote);

module.exports = router;
