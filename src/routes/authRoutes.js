const express = require('express');
const router = new express.Router();
const authControllers = require('../controllers/AuthControllers');

router.post('/register', authControllers.postRegister);
router.post('/login', authControllers.postLogin);

module.exports = router;
