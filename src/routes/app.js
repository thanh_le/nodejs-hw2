const userRoutes = require('./userRoutes');
const authRoutes = require('./authRoutes');
const noteRoutes = require('./noteRoutes');
const verifyToken = require('../middlewares/auth');

module.exports = function router(app) {
  app.use('/api/users', verifyToken, userRoutes);
  app.use('/api/auth', authRoutes);
  app.use('/api/notes', verifyToken, noteRoutes);
};
