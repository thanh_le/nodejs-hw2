const express = require('express');
const router = new express.Router();
const userController = require('../controllers/UserControllers');
const verifyToken = require('../middlewares/auth');

router.get('/me', verifyToken, userController.getUser);
router.patch('/me', verifyToken, userController.updateUser);
router.delete('/me', verifyToken, userController.deleteUser);

module.exports = router;
