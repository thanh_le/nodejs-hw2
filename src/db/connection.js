const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

const DB_URI = process.env.DB_URI;

const dbConnection = async () => {
  await mongoose.connect(DB_URI, {});
  console.log('Connect to db successfully!');
};

module.exports = dbConnection;
